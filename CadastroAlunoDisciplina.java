import java.io.*;

//Criação da Classe Pública CadastroAlunoDisciplina
public class CadastroAlunoDisciplina{
  
	public static void main (String [] args) throws IOException{
		
		//Burocracia para leituda do teclado
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader (entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader (leitor);
		String entradaTeclado;

    		//Instanciação dos Objetos
 	   	Aluno umAluno;
	 	Disciplina umaDisciplina;
		ControleAluno umControleAluno = new ControleAluno();
		ControleDisciplina umControleDisciplina = new ControleDisciplina();
    
		//Criação do Menu para interagir com usuário
		char menu;
		
		do{
     			System.out.println("\n\n==============MENU==============");
			System.out.println("\nDigite 1 para cadastrar nova Disciplina");
			System.out.println("Digite 2 para cadastrar novo Aluno");
      			System.out.println("Digite 3 para vincular um Aluno a uma Disciplina");
			System.out.println("Digite 4 para remover Disciplina");
			System.out.println("Digite 5 para remover Aluno");
			System.out.println("Digite 6 para listar Disciplinas");
			System.out.println("Digite 7 para listar todos Alunos");
  			System.out.println("Digite 8 para listar Alunos de uma Disciplina Específica");
      			System.out.println("Digite 0 para sair");
			System.out.println("\nDigite a opcao desejada: ");

			entradaTeclado = leitorEntrada.readLine();
			menu = entradaTeclado.charAt(0);
			
     			System.out.println("");
      
			if(menu == '1'){
				System.out.println("Digite o nome da Disciplina: ");
				String nomeDisciplina = leitorEntrada.readLine();
        
				System.out.println("Digite o codigo da Disciplina: ");
				String codigoDisciplina = leitorEntrada.readLine();
        
				umaDisciplina = new Disciplina(nomeDisciplina, codigoDisciplina);
        			//Chamada da função em ControleDisciplina para adicionar nova disciplina
				umControleDisciplina.adicionar(umaDisciplina);
      }
			else if(menu == '2'){
    		 		System.out.println("Digite o nome do Aluno: ");
    				String nomeAluno = leitorEntrada.readLine();
          
       	 			System.out.println("Digite a matricula do Aluno: ");
        			String matriculaAluno = leitorEntrada.readLine();
          
        			umAluno = new Aluno(nomeAluno, matriculaAluno);
        
      				//Chamada da função em ControleAluno para adicionar novo aluno
     				umControleAluno.adicionar(umAluno);
     			}
      			else if (menu == '3'){
        			//Chamada das funções para verificar se há aluno e disciplina cadastrados
        			if(umControleAluno.pesquisarGeral()==0){
          				System.out.println("Nenhum aluno fora cadastrado!");
       				}
        			else if(umControleDisciplina.pesquisarGeral()==0){
          				System.out.println("Nenhuma disciplina fora cadastrada!");
        }
        
        			else{
          				System.out.println("Alunos Cadastrados:");
          				umControleAluno.pesquisarExibir();
          				System.out.println("\nDisciplinas Cadastradas:");
          				umControleDisciplina.pesquisarExibir();
          
         	 			System.out.println("\nDigite o nome do Aluno a ser matriculado: ");
          				String umNomeAluno = leitorEntrada.readLine();
          				umAluno = umControleAluno.pesquisar(umNomeAluno);
        
          				System.out.println("Digite o nome da Disciplina: ");
          				String umNomeDisciplina = leitorEntrada.readLine();
          				umaDisciplina = umControleDisciplina.pesquisar(umNomeDisciplina);
          
          				//Chamada da função em ControleDisciplina para vincular aluno à disciplina
          				umControleDisciplina.matricularAluno(umAluno,umaDisciplina);
          
          				System.out.println("\nAluno '" + umNomeAluno + "' vinculado com sucesso à disciplina '" + umNomeDisciplina + "'!");
        			}
      			}
			else if(menu == '4'){
        			if(umControleDisciplina.pesquisarGeral()==0){
          				System.out.println("Nenhuma disciplina fora cadastrada!");
        			}
        			else{
          				System.out.println("Disciplinas Cadastradas:");
          				umControleDisciplina.pesquisarExibir();
          
         				System.out.println("\nDigite o nome da Disciplina a ser removida: ");
          				String nomeDisciplina = leitorEntrada.readLine();
          				umaDisciplina = umControleDisciplina.pesquisar(nomeDisciplina);
          
        	  			//Chamada da função em ControleDisciplina para remover Disciplina existente
          				umControleDisciplina.remover(umaDisciplina);
          
        			}
			}
      			else if (menu == '5'){
        			if(umControleAluno.pesquisarGeral()==0){
        	  			System.out.println("Nenhum aluno fora cadastrado!");
       				}
        			else{
          				System.out.println("Alunos Cadastrados: ");
          				umControleAluno.pesquisarExibir();
          
          				System.out.println("\nDigite o nome do Aluno a ser removido: ");
          				String nomeAluno = leitorEntrada.readLine();
          				umAluno = umControleAluno.pesquisar(nomeAluno);
          
          				//Chamada da função em ControleAluno para remover Aluno existente
          				umControleAluno.remover(umAluno);
        			}
      			}
      			else if (menu == '6'){
        			if(umControleDisciplina.pesquisarGeral()==0){
          				System.out.println("Nenhuma disciplina fora cadastrada!");
        			}
        			else{
        				umControleDisciplina.pesquisarExibir();
       			 	}
      			}
      			else if (menu == '7'){
        			if(umControleAluno.pesquisarGeral()==0){
          				System.out.println("Nenhum aluno fora cadastrado!");
       	 			}
        			else{
          				umControleAluno.pesquisarExibir();
        			}
      			}
      			else if (menu == '8'){
        			if(umControleAluno.pesquisarGeral()==0){
        				System.out.println("Nenhum aluno fora cadastrado!");
        			}
        			else if(umControleDisciplina.pesquisarGeral()==0){
          				System.out.println("Nenhuma disciplina fora cadastrada!");
        			}
        			else{
          				System.out.println("Disciplinas Cadastradas:");
          				umControleDisciplina.pesquisarExibir();
        
          				System.out.println("\nDigite o nome da Disciplina desejada: ");
          				String umaMateria = leitorEntrada.readLine();
        
          				umaDisciplina = umControleDisciplina.pesquisar(umaMateria);
          				int y = umControleDisciplina.pesquisarExibirAlunos(umaDisciplina);
          				if(y==0){
            					System.out.println("\nNenhum Aluno fora cadastrado em '" + umaMateria + "' ainda");
          				}
        			}
      			}
      			else{
        			System.out.println("Essa opção não existe");
      			}
    		}while (menu != '0');
	}
}	
