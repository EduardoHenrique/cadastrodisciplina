import java.util.ArrayList;

//Classe pública Disciplina
public class Disciplina{
	
	//Atributos
	private String nome;
	private ArrayList<Aluno> listaAlunos;
	private String codigo;
  
  	//Construtores
  	public Disciplina(){
  	}
  
	public Disciplina (String umNome, String umCodigo) {
    		nome = umNome;
    		codigo = umCodigo;
    		listaAlunos = new ArrayList<Aluno>();
	}
  
 	//Métodos
	public void setNome(String umNome){
		nome = umNome;
	}

	public String getNome(){	
		return nome;
	}

	public void setCodigo(String umCodigo){
		codigo = umCodigo;
	 }

	public String getCodigo(){
		return codigo;
	}
  
	public void setListaAlunos (Aluno umAluno){
		listaAlunos.add(umAluno);
	}
  
  	public int exibirAlunos(Disciplina umaDisciplina){
    		int y=0;
    		for(Aluno umAluno : umaDisciplina.listaAlunos){
      			System.out.println("Nome: '" + umAluno.getNome() + "' - Matricula: " +umAluno.getMatricula());
      			y = y + 1;
    		}
    		return y;
	}
}
