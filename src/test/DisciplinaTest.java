package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Disciplina;


public class DisciplinaTest {
	
	private Disciplina umaDisciplina;

	@Before
	public void setup() throws Exception{
		umaDisciplina = new Disciplina();
	}
	
	@Test
	public void setNomeTest(){
		umaDisciplina.setNome("Orientação a Objetos");
		assertEquals("Orientação a Objetos", umaDisciplina.getNome());
	}
	@Test
	public void setCodigoTest(){
		umaDisciplina.setCodigo("1234556");
		assertEquals("1234556", umaDisciplina.getCodigo());
	}
	
}