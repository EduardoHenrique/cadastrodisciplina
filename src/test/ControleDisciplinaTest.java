package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Disciplina;
import controller.ControleDisciplina;


public class ControleDisciplinaTest {
	
	private Disciplina umaDisciplina;
	private ControleDisciplina umControleDisciplina;
	
	@Before
	public void setup() throws Exception{
		umaDisciplina = new Disciplina();
		umControleDisciplina = new ControleDisciplina();
	}
	
	@Test
	public void adicionarDisciplinaTest(){
		umaDisciplina.setNome("Orientação a Objetos");
		umaDisciplina.setCodigo("123456");
		//umAluno = new Aluno("Eduardo","13/0008371");
		umControleDisciplina.adicionar(umaDisciplina);
		assertEquals(umaDisciplina,umControleDisciplina.pesquisar("Orientação a Objetos"));
	}
}