package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Aluno;


public class AlunoTest {
	
	private Aluno umAluno;

	@Before
	public void setup() throws Exception{
		umAluno = new Aluno();
	}
	
	@Test
	public void setNomeTest(){
		umAluno.setNome("Eduardo");
		assertEquals("Eduardo", umAluno.getNome());
	}
	@Test
	public void setMatriculaTest(){
		umAluno.setMatricula("13/0008371");
		assertEquals("13/0008371", umAluno.getMatricula());
	}
	
}