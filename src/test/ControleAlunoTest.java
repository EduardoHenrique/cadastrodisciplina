package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import controller.ControleAluno;
import model.Aluno;


public class ControleAlunoTest {
	
	private ControleAluno umControleAluno;
	private Aluno umAluno;

	@Before
	public void setup() throws Exception{
		umControleAluno = new ControleAluno();
		umAluno = new Aluno();
	}
	
	@Test
	public void adicionarAlunoTest(){
		umAluno.setNome("Eduardo");
		umAluno.setMatricula("13/0008371");
		//umAluno = new Aluno("Eduardo","13/0008371");
		umControleAluno.adicionar(umAluno);
		assertEquals(umAluno,umControleAluno.pesquisar("Eduardo"));
	}
	
	@Test
	public void removerAlunoTest(){
		umAluno.setNome("Eduardo");
		umAluno.setMatricula("13/0008371");
		umControleAluno.adicionar(umAluno);
		umControleAluno.remover(umAluno);
		assertEquals(null,umControleAluno.pesquisar("Eduardo"));
	}
	
}