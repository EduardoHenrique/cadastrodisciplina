import java.util.ArrayList;

public class ControleDisciplina{
	//Atributos
	private ArrayList<Disciplina> listaDisciplinas;
	
	//Construtor
	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();
	}

	//Métodos
	public void adicionar (Disciplina umaDisciplina){
		listaDisciplinas.add(umaDisciplina);
	}

	public void remover (Disciplina umaDisciplina){
		listaDisciplinas.remove(umaDisciplina);
	}
  
  	public void matricularAluno (Aluno umAluno,Disciplina umaDisciplina){
    		umaDisciplina.setListaAlunos(umAluno);
  	}

	public Disciplina pesquisar(String nomeDisciplina){
		for(Disciplina umaDisciplina: listaDisciplinas){
			if(umaDisciplina.getNome().equalsIgnoreCase(nomeDisciplina)){
				return umaDisciplina;
			}
		}
    	return null;
	}
  
  	public int pesquisarGeral(){
    		int x=0;
		for(Disciplina umaDisciplina: listaDisciplinas){
      			x = x + 1;
    		}
    		return x;
	}
  
  	public void pesquisarExibir(){
    		for(Disciplina umaDisciplina:listaDisciplinas){
      			System.out.println ("Disciplina: " + umaDisciplina.getNome() + " - Código: " + umaDisciplina.getCodigo());
    		}
  	}
  
  	public int pesquisarExibirAlunos(Disciplina umaDisciplina){
    		int y = umaDisciplina.exibirAlunos(umaDisciplina);
    		return y;
  	}
}

